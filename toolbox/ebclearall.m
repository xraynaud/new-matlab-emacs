function ebclearall()
% Emacs version of dbstop.  Tells emacs which breakpoints are active.
    
    dbclear('all');
    
    % Send emacs some breakpoints
    bp = getappdata(groot, 'EmacsBreakpoints');
    bp.resetEmacs;

end

